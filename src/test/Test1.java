package test;

import streams.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Test1 {
    public static void main(String[] args) {

        User user1 = new User("Kolya", "ddd@gmail.com", 25, 7);
        User user2 = new User("Vasya", "Vasya@mail.com", 30, 9);
        User user3 = new User("Lena", "Lena@yandex.com", 20, 3);
        User user4 = new User("Julia", "Julia@gmail.com", 23, 5);
        User user5 = new User("Semen", "Semen@gmail.com", 45, 2);

        List<User> userList = new ArrayList<>();
        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
        userList.add(user4);
        userList.add(user5);

        userList = userList.stream().filter(x -> x.getAge() <= 30 && x.getRating() > 4).collect(Collectors.toList());
//        for (User i : userList) {
//            System.out.println(i);
//        }

        userList.stream().forEach(System.out::println);

        Integer sd = userList.stream().map(x -> x.getRating()).peek(System.out::println).reduce((x, y) -> x + y).get();
        System.out.println(sd);

        userList.stream().forEach(System.out::println);

        User d = userList.stream().filter(x -> x.getAge() < 40).findFirst().get();
        System.out.println("\n"+d);
    }
}
