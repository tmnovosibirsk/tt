package streams;

public class User {
    private String name;
    private String email;

    private int age;

    private int rating;

    public void setAge(int age) {
        this.age = age;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getAge() {
        return age;
    }

    public int getRating() {
        return rating;
    }


    @Override
    public String toString() {
        return "streams.User{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                ", rating=" + rating +
                '}';
    }

    public User(String name, String email, int age, int rating) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.rating = rating;
    }

    public User(String name, String email) {
        this.name = name;
        this.email = email;
    }

    // Getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}