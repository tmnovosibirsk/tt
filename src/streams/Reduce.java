package streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Reduce {
    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();

        list.add(5);
        list.add(2);
        list.add(7);

        int sd = list.stream().reduce((a, x) -> a + x).get();
        //Если используем reduce без первого значения, то получаем значение Optional, для которого используем метод get()
        Optional<Integer> o = list.stream().reduce((a, x) -> a + x);

        int v = list.stream().reduce(1, (a, x) -> a * x);

        int vv = list.stream().reduce(3, (a, x) -> a * x);
        //1 - это число аккамулятор с которым начинаем умножение
        //Если используем reduce с первым значением, тогда get() не требуется, сразу получаем инт

        System.out.println("Сложение " + sd);
        System.out.println("Значение с аккамулятором 1 :  " + v);
        System.out.println("Значение с аккамулятором 3 :  " + vv);
        System.out.println("Это Optional : " + o.get());

    }
}
