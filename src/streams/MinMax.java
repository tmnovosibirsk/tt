package streams;

import java.util.ArrayList;
import java.util.List;

public class MinMax {
    public static void main(String[] args) {
        User user1 = new User("Kolya", "ddd@gmail.com", 25, 7);
        User user2 = new User("Vasya", "Vasya@mail.com", 30, 9);
        User user3 = new User("Lena", "Lena@yandex.com", 20, 3);
        User user4 = new User("Julia", "Julia@gmail.com", 23, 5);
        User user5 = new User("Semen", "Semen@gmail.com", 45, 2);

        List<User> list = new ArrayList<>();
        list.add(user1);
        list.add(user2);
        list.add(user3);
        list.add(user4);
        list.add(user5);

        User min = list.stream().min((x,y) -> x.getAge()-y.getAge()).get();
        System.out.println(min);
        User max = list.stream().max((x,y) -> x.getAge()-y.getAge()).get();
        System.out.println(max);

        List<Integer> list2 = new ArrayList<>();
        list2.add(2);
        list2.add(5);
        list2.add(10);
        list2.add(15);

        int d =  list2.stream().min((x,y)->x-y).get();
        int s =  list2.stream().max((x,y)->x-y).get();

        System.out.println(d);
        System.out.println(s);
    }
}
