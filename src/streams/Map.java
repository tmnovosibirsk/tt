package streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Map {
    public static void main(String[] args) {

        List<String> list = new ArrayList<>();

        list.add("privet");
        list.add("kak dela?");
        list.add("poka");
        list.add("ok");
        list.add("dotvidania");

//        List<Integer> list2 = list.stream().map(x -> x.length())
//                .collect(Collectors.toList());
//
//        System.out.println("Длина слов из списка " + list2);

        list = list.stream().map(x -> {
            if (x.equals("privet") || x.equals("kak dela?")) {
                x = ("Новое значение " + x.toUpperCase());
            }
            return x;
        }).collect(Collectors.toList());


        for (String i : list) {
            System.out.println("[" + i + "]");
        }



//        int[] array = {5, 9, 3, 8, 6};
//        int[] sdd = Arrays.stream(array).map(x -> {
//            if (x % 3 == 0) {
//                return x / 3;
//            }
//            return 0;
//        }).toArray();
//
//        for (int i : sdd) {
//            System.out.println(i);
//        }

            int[] array = {5, 9, 3, 8, 6};
        array = Arrays.stream(array).map(x -> {
            if (x % 3 == 0) {
                x = x / 3;
            }
            return x;
        }).toArray();

//        for (int i : array) {
//            System.out.println(i);
//        }
        System.out.println(Arrays.toString(array));
    }

}
