package streams;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class filter {
    public static void main(String[] args) {
        User user1 = new User("Kolya", "ddd@gmail.com", 25, 7);
        User user2 = new User("Vasya", "Vasya@mail.com", 30, 9);
        User user3 = new User("Lena", "Lena@yandex.com", 20, 3);
        User user4 = new User("Julia", "Julia@gmail.com", 23, 5);
        User user5 = new User("Semen", "Semen@gmail.com", 45, 2);

        List<User> userList = new ArrayList<>();
        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
        userList.add(user4);
        userList.add(user5);

        System.out.println(userList);

        userList = userList.stream().filter(x ->
                x.getRating() > 5 && x.getAge() >=25).collect(Collectors.toList());

        long count = userList.stream().filter(x ->
                x.getRating() > 5 && x.getAge() >=25).count();

        System.out.println(userList);

        System.out.println("Количество выданных результатов " + count);




    }
}



