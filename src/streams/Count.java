package streams;

import java.util.Arrays;

public class Count {
    public static void main(String[] args) {
        int[] array = {5, 9, 5, 7, 2};

        long d = Arrays.stream(array).filter(x->x==5).count();
        System.out.println(d);
    }
}
