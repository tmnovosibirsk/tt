package streams;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class forEach {
    public static void main(String[] args) {
        User user1 = new User("Kolya", "ddd@gmail.com", 25, 7);
        User user2 = new User("Vasya", "Vasya@mail.com", 30, 9);
        User user3 = new User("Lena", "Lena@yandex.com", 20, 3);
        User user4 = new User("Julia", "Julia@gmail.com", 23, 5);
        User user5 = new User("Semen", "Semen@gmail.com", 45, 2);

        List<User> list = new ArrayList<>();
        list.add(user1);
        list.add(user2);
        list.add(user3);
        list.add(user4);
        list.add(user5);

        list.stream().forEach(x -> System.out.println(x));
        System.out.println("   ");
        int[] array = {5, 9, 3, 8, 6};

        Arrays.stream(array).forEach(x -> {
            x *= 2;
            System.out.println(x);
        });

        Stream<User> stream = Stream.of(user1, user2, user3, user4, user5);
        stream.forEach(x -> {
            int d = x.getAge() * 2;
            x.setAge(d);
            System.out.println(x.getAge());
        });

    }
}
