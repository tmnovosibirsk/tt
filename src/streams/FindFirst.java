package streams;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FindFirst {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();

        list.add("privet");
        list.add("kak dela?");
        list.add("poka");
        list.add("ok");
        list.add("dotvidania");
        list.add("dotvidania");

        int [] age = {1,2,43,5,6};
        System.out.println(Arrays.toString(age));


        list.stream().distinct().filter(x->x.length()>2).sorted().peek(x-> System.out.println(x + " ETO PEEK")).forEach(System.out::println);

//        Boolean d = list.stream().filter(x -> x.equals("poka")).findAny().get().contains("p");
//        System.out.println(d);
//
//        list.removeIf(x -> x.equals("privet") || x.equals("poka"));
//        System.out.println(list);
//
//        list.removeIf(x -> x.length() < 6);
//        System.out.println(list);


    }

}
